
    val subscription = new RecordBuffer(RecordMode.HISTORY_SUBSCRIPTION);
    

    val file = new File("src/main/resources/full-order-depth.csv")

    val path = file.getAbsolutePath
    val conn = DXEndpoint.create().connect(s"file:$path").asInstanceOf[DXEndpointImpl]
    val feed = conn.getFeed()
    val sub = feed.createSubscription(classOf[Order])
    sub.addEventListener(new DXFeedEventListener[Order]() {
      def eventsReceived(events: java.util.List[Order]) = {
        for (quote <- events.asScala)
          println(quote)
      }
    })
    sub.addSymbols("AAPL")
    sub.attach(feed)
    
    //.addMessageConnectionListener(x => println(x))
    //println(conn.getContracts())

    //println("HELLO")
/*
    // save file to byte array in scala
    val buffer = new Array[Byte](file.length().toInt)
    val in = new java.io.FileInputStream(file)
    val length = in.read(buffer)

    val parser = FileFormat.detectFormat(buffer).createQTPParser(QDFactory.getDefaultScheme())
*/
    /*
    val parser = FileFormat.detectFormat(buffer).createQTPParser(QDFactory.getDefaultScheme())
    parser.setInput(new ByteArrayInput(buffer, 0, length))
    parser.parse(new MessageConsumerAdapter() {
      override def processTickerAddSubscription(iterator: SubscriptionIterator) = {
        subscription.processSubscription(iterator)
      }
    })*/
    //QDLog.log.info("Done reading subscription from " + LogUtil.hideCredentials(filename) + ": read " + subscription.size() + " //elements")

    // save file to byte array in java

/*
byte[] buffer = new byte[(int) file.length()];
        int length = 0;
        try (FileInputStream in = new FileInputStream(file)) {
            length = in.read(buffer);
        } catch (IOException e) {
            QDLog.log.error("Error reading subscription", e);
        }
        AbstractQTPParser parser = FileFormat.detectFormat(buffer).createQTPParser(QDFactory.getDefaultScheme());
        parser.setInput(new ByteArrayInput(buffer, 0, length));
        parser.parse(new MessageConsumerAdapter() {
            @Override
            public void processTickerAddSubscription(SubscriptionIterator iterator) {
                subscription.processSubscription(iterator);
            }
        });
        QDLog.log.info("Done reading subscription from " + LogUtil.hideCredentials(filename) + ": read " + subscription.size() + " elements");
    */

    /*
    val feed = DXEndpoint.create().connect(s"file:$path").getFeed()

        
    val model = new OrderBookModel()
    model.setFilter(OrderBookModelFilter.ALL)
    model.setSymbol("AAPL")

    model.addListener(new OrderBookModelListener() {
        def modelChanged(change: OrderBookModelListener.Change) = {
            println("Buy orders:")
            for (order <- model.getBuyOrders.asScala)
                println(order)
            println("Sell orders:")
            for (order <- model.getSellOrders.asScala)
                println(order)
            println()
        }
    });
    model.attach(feed);

    */
    /*
    val sub = feed.createSubscription(classOf[Trade])
    sub.addEventListener(new DXFeedEventListener[Trade]() {
      def eventsReceived(events: java.util.List[Trade]) = {
        for (quote <- events.asScala)
          println(quote)
      }
    })
    sub.addSymbols("AAPL")*/
    while (!Thread.interrupted())
        Thread.sleep(1000)
    /*sub.addEventListener(new DXFeedEventListener() {
            public void eventsReceived(List events) {
                    for (Quote quote : events)
                            System.out.println(quote);
            }
    });
                sub.addSymbols(symbol);
                while (!Thread.interrupted())
                        Thread.sleep(1000);
    *//*
            
        DXFeed feed = ...;
        OrderBookModel model = new OrderBookModel();
        model.setFilter(OrderBookModelFilter.ALL);
        model.setSymbol("AAPL");
        model.addListener(new OrderBookModelListener() {
            public void modelChanged(OrderBookModelListener.Change change) {
                System.out.println("Buy orders:");
                for (Order order : model.getBuyOrders())
                    System.out.println(order);
                System.out.println("Sell orders:");
                for (Order order : model.getSellOrders())
                    System.out.println(order);
                System.out.println();
            }
        });
        model.attach(feed);
    
    */
    println("Hello, world!")

    /*
    def working = {
        val subscription = new RecordBuffer(RecordMode.TIMESTAMPED_DATA)        
        val scheme = QDFactory.getDefaultScheme()
        
        val file = new File("src/main/resources/full-order-depth.csv")
        val buffer = new Array[Byte](file.length().toInt)
        val in = new java.io.FileInputStream(file)
        val length = in.read(buffer)
        val parser = new TextQTPParser(scheme, MessageType.STREAM_DATA);
        parser.setDelimiters(TextDelimiters.COMMA_SEPARATED);

        parser.setInput(new ByteArrayInput(buffer, 0, length))
        parser.parse(new MessageConsumerAdapter() {
            override def processData(iterator: DataIterator, message:  MessageType) {
                subscription.processData(iterator)
            }
        })

        while (subscription.hasNext()) {
            val cursor = subscription.next()
            println(cursor.toString())
            println(cursor.getSymbol())
        }

    }
*/