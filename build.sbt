name := "pcm-dxfeed"
version := "1.0"
scalaVersion := "2.13.10"


resolvers += "dxfeed" at "https://dxfeed.jfrog.io/artifactory/maven-open"


libraryDependencies ++= Seq(
   "com.devexperts.qd" % "dxlib" % "3.316",
   "com.devexperts.qd" % "dxfeed-api" % "3.316",
   "com.devexperts.qd" % "qds" % "3.316",
   "com.devexperts.qd" % "qds-tools" % "3.316"
)
