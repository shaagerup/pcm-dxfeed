package pcm

import com.devexperts.transport.stats.ConnectionStats
import com.devexperts.qd.qtp.file.FileReaderParams
import com.devexperts.qd.ng.RecordMode
import com.devexperts.qd.QDFactory
import com.devexperts.qd.qtp.file.FileReader
import com.devexperts.qd.qtp.MessageConsumerAdapter
import com.devexperts.qd.DataIterator
import com.devexperts.qd.ng.RecordBuffer
import com.devexperts.qd.qtp.MessageType
import com.dxfeed.event.market.Trade
import com.dxfeed.event.market.impl.TradeMapping
import com.dxfeed.event.market.impl.OrderMapping
import com.dxfeed.event.market.OrderDelegate
import com.devexperts.qd.QDContract
import java.util.EnumSet
import com.dxfeed.api.impl.EventDelegateFlags
import com.dxfeed.api.DXEndpoint
import com.dxfeed.model.market.OrderBookModel
import java.util.Collections
import com.dxfeed.api.impl.DXEndpointImpl
import com.dxfeed.event.market.Order
import com.dxfeed.event.market.Scope
import com.dxfeed.model.AbstractIndexedEventModel
import com.dxfeed.model.IndexedEventModel
import java.{util => ju}
import com.dxfeed.api.DXFeedSubscription
import scala.collection.mutable.ArrayBuffer
object Main {

  def main(args: Array[String]) = {
    // todo: Figure out how to make use of DXFeed, Delegate etc. to use OrderBookModel
    val path = "src/main/resources/full-order-depth.csv"
    val stats = new ConnectionStats()

    val params = new FileReaderParams.Default()
    params.setSpeed(FileReaderParams.MAX_SPEED)

    val reader = new FileReader(path, stats, params)
    val recordMode = RecordMode.DATA.withEventFlags().withEventTimeSequence().withTimeMark().withLink()
    val scheme = QDFactory.getDefaultScheme()

    // single threaded -- execute in place
    val endpoint = DXEndpoint.create(DXEndpoint.Role.LOCAL_HUB).executor(r => r.run()).asInstanceOf[DXEndpointImpl]
    val distributor = endpoint.getCollector(QDContract.HISTORY).distributorBuilder().build()

    // WORKING
    // mutable scala map
    val m = scala.collection.mutable.Map[Long, scala.collection.mutable.ArrayBuffer[(Long, Double)]]()
    val sub = new DXFeedSubscription(classOf[Order])
    sub.addEventListener(events =>
      events.forEach(event =>
        println(
          m.updateWith(event.getIndex())(x => Some(x.getOrElse(ArrayBuffer()) += ((event.getSize(), event.getPrice()))))
        )
      // println(event.getIndex())
      // println(order.hasSize() || order.getScope() == Scope.COMPOSITE)
      // println(order)
      )
    )
    sub.addSymbols("AAPL")
    endpoint.getFeed().attachSubscription(sub)

    Thread.sleep(10000)
    println(m)

    /*
    // Also working
    val sub = endpoint.getFeed().createSubscription(classOf[Order])
    sub.addEventListener(events =>
      events.forEach(order => println(order.hasSize() || order.getScope() == Scope.COMPOSITE))
    )
    sub.addSymbols("AAPL")
     */
    /*
    // NOT WORKING
    val model = new OrderBookModel()
    model.setSymbol("AAPL")

    model.getBuyOrders().addListener(event => println("Buy order changed"))
    model.getSellOrders().addListener(event => println("Sell order changed"))
    model.addListener(event => println("Model changed"))
    // model.setExecutor(r => r.run())
    model.attach(endpoint.getFeed())
     */
    /*
    // NOT WORKING
    val model = new IndexedEventModel(classOf[Order]) // new TestModel()
    model.setSymbol("AAPL")
    model.getEventsList().addListener(change => println(change))

    model.setExecutor(r => r.run())
    model.attach(endpoint.getFeed())
     */
    reader.readInto(new MessageConsumerAdapter() {
      override def processData(iterator: DataIterator, message: MessageType) {
        val buf = new RecordBuffer(recordMode)

        buf.processData(iterator)
        distributor.process(buf)
        buf.release()

      }
    })
  }
}
